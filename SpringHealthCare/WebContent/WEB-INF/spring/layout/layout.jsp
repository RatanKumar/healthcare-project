<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <tiles:insertAttribute name="static" />
 <title><tiles:insertAttribute name="title" ignore="true" /></title>
 
</head>
<body>
  <div class="maindiv">
      <div class="header1">
        <tiles:insertAttribute name="header" />
      </div>
      
      <div class="menu">
        <tiles:insertAttribute name="menu" />
      </div>
      
      <div class="contents" >
        <div style="width: 75%;float: left; width: 500px;"> 
           <tiles:insertAttribute name="body" />
        </div>
        
         <div style="float: right; width: 25%;border-style:solid; height: 500px;border-radius:5px;">
            <tiles:insertAttribute name="notify" />
        </div>
      </div>
      
      <div class="footer">
          <tiles:insertAttribute name="footer" />
      </div>
  
  </div>


</body>
</html>