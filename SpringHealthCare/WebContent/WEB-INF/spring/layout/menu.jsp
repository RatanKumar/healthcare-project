<div id="menu">
    <ul class="menu">
        <li><a href="home.htm"><span>About us</span></a></li>
        <li><a href="faculty.htm"><span>Patient Care</span></a></li>
        <li><a href="parents.htm" class="parent"><span>Services &amp; Facilities</span></a>
            <div><ul>
                <li><a href="parents.htm"><span>Notifications</span></a> </li>
                <li><a href="#"><span>Entrance Test</span></a></li>
                <li><a href="#"><span>Additions</span></a></li>
            </ul></div>
        </li>
        <li><a href="#" class="parent"><span>Appointment</span></a>
            <div><ul>
                <li><a href="parents.htm"><span>Time Tables</span></a> </li>
                <li><a href="#"><span>Exam Results</span></a></li>
                <li><a href="#"><span>Previous Papers</span></a></li>
            </ul></div>
        </li>
        <li><a href="#" class="parent"><span>Find a Doctor</span></a>
            <div><ul>
                <li><a href="#"><span>Transportation</span></a> </li>
                <li><a href="#"><span>Computer Lab</span></a></li>
                <li><a href="#"><span>Science Lab</span></a></li>
                <li><a href="#"><span>In Door Games</span></a></li>
                <li><a href="#"><span>Out Door Games</span></a></li>
            </ul></div>
        </li>
        <!-- <li><a href="admin.htm"><span>Admin</span></a></li>
        <li><a href="parents.htm"><span>Parents</span></a></li>
        <li><a href="#"><span>Help</span></a></li>  -->
        <li><a href="#" class="parent"><span>Gallery</span></a>
            <div><ul>
                <li><a href="#"><span>Independence Day</span></a> </li>
                <li><a href="#"><span>Annual Day</span></a></li>
                <li><a href="#"><span>Sub Item 3</span></a></li>
            </ul></div>
        </li>
        <li class="last"><a href="contactus.htm"><span>Contact Us</span></a></li>
        <li class="last"><a href="staff.html"><span>Staff</span></a></li>
    </ul>
</div>
