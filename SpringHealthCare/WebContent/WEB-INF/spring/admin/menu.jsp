<div id="menu">
    <ul class="menu">
    	<li><a href="#" class="parent"><span>Staff</span></a>
            <div><ul>
                <li><a href="sCreate.htm"><span>Create</span></a> </li>
                <li><a href="sUpdate.htm"><span>Update</span></a></li>
                <li><a href="sView.htm"><span>View</span></a></li>
            </ul></div>
        </li>
        <li><a href="#" class="parent"><span>Service Master</span></a>
            <div><ul>
                <li><a href="#"><span>Create</span></a> </li>
                <li><a href="#"><span>Update</span></a></li>
                <li><a href="#"><span>View</span></a></li>
            </ul></div>
        </li>
         <li><a href="#" class="parent"><span>Lab Master</span></a>
            <div><ul>
                <li><a href="#"><span>Units</span></a> </li>
                <li><a href="#"><span>Parameter</span></a></li>
                <li><a href="#"><span>Test Configuration</span></a></li>
                <li><a href="#"><span>Lab Templates</span></a></li>
            </ul></div>
        </li>
         <li><a href="#" class="parent"><span>Pharmacy Master</span></a>
            <div><ul>
                <li><a href="#"><span>Add Event</span></a> </li>
                <li><a href="#"><span>View Event</span></a></li>
            </ul></div>
        </li>
            <li><a href="#" class="parent"><span>IP Billing Master</span></a>
            <div><ul>
                <li><a href="#"><span>Service Billing</span></a> </li>
                <li><a href="#"><span>View Event</span></a></li>
            </ul></div>
        </li>
             <li><a href="#" class="parent"><span>OP Billing Master</span></a>
            <div><ul>
                <li><a href="#"><span>Add Event</span></a> </li>
                <li><a href="#"><span>View Event</span></a></li>
            </ul></div>
        </li>
        
        <li class="last"><a href="#"><span>Doctor Appointment Master</span></a></li>
        <li class="last"><a href="#"><span>Reports</span></a></li>
    </ul>
</div>
