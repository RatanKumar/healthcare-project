<div id="menu">
    <ul class="menu">
        <li><a href="labserach.htm" class="parent"><span>Lab</span></a>
            <div><ul>
                <li><a href="labsearch.htm"><span>Lab Search</span></a> </li>
                <li><a href="samplecollection.htm"><span>Sample Collection</span></a></li>
                 <li><a href="resultsentry.htm"><span>Results Entry</span></a> </li>
                <li><a href="resultsverify.htm"><span>Results Verification</span></a></li>
                 <li><a href="reportgen.htm"><span>Report generation</span></a> </li>
                <li><a href="reportdispatch.htm"><span>Report Dispatching</span></a></li>
            </ul></div>
        </li>
         <li><a href="staffregister.html"><span>Registration</span></a>
            <div><ul>
                <li><a href="staffregister.html"><span>Create</span></a></li>
                <li><a href="staffupadate.html"><span>Update</span></a></li>
                 <li><a href="staffview.html"><span>Cancel</span></a></li>
            </ul></div>
        </li>
        <li><a href="#" class="parent"><span>OP Billing</span></a>
            <div><ul>
                <li><a href="#"><span>Bill Search</span></a></li>
                 <li><a href="#"><span>Bill Generation</span></a></li>
                  <li><a href="#"><span>Report Generation</span></a></li>
            </ul></div>
        </li>
         <li><a href="#" class="parent"><span>Ip </span></a>
            <div><ul>
                <li><a href="#"><span>IP Billing Generation</span></a> </li>
                <li><a href="#"><span>Ip Bill Payment</span></a> </li>
            </ul></div>
        </li>
         <li><a href="#" class="parent"><span>Patient</span></a>
            <div><ul>
                <li><a href="#"><span>Patient Search</span></a> </li>
                <li><a href="#"><span></span></a> </li>
            </ul></div>
        </li>
        <li class="last"><a href="#"><span>Reports</span></a></li>
    </ul>
</div>
