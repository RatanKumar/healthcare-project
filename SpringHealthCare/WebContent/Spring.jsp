<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META HTTP-EQUIV="refresh" CONTENT="10;URL=home.html">
<title>Spring Application</title>
</head>
<body>
  <div>
   <h3>Struts Framework flow</h3>
   <a href="home.html">Go To Struts</a>
  </div>
  
  <div>
     <h3>Spring Framework flow</h3>
      <a href="home.htm">Go To Spring</a>
  </div>
 <!--  <div>
     <h4> Hibernate ORM flow </h4>
     <a href="home.htm"> Go To Hibernate </a>
  </div> -->

</body>
</html>
