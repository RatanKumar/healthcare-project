package org.venu.jdbc.domain;

import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.sql.Timestamp;


public class LoginUser {

	private int userNbr;
	private String username;
	private String password;
	private String firstname;
	private String lastname;
	private String email;
	private long phoneNbr;
	private String userAddress;
	private char activeIND ;
	private Timestamp  createDate;
	private Timestamp  ModifyDate;
	private InputStream image_in;
	private FileOutputStream image_out;
	private FileReader text_reader;
	private FileWriter text_writer;
	
	
	public InputStream getImage_in() {
		return image_in;
	}
	public void setImage_in(InputStream image_in) {
		this.image_in = image_in;
	}
	public FileOutputStream getImage_out() {
		return image_out;
	}
	public void setImage_out(FileOutputStream image_out) {
		this.image_out = image_out;
	}
	public FileReader getText_reader() {
		return text_reader;
	}
	public void setText_reader(FileReader text_reader) {
		this.text_reader = text_reader;
	}
	public FileWriter getText_writer() {
		return text_writer;
	}
	public void setText_writer(FileWriter text_writer) {
		this.text_writer = text_writer;
	}
	
	public int getUserNbr() {
		return userNbr;
	}
	public void setUserNbr(int userNbr) {
		this.userNbr = userNbr;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public long getPhoneNbr() {
		return phoneNbr;
	}
	public void setPhoneNbr(long phoneNbr) {
		this.phoneNbr = phoneNbr;
	}
	public char getActiveIND() {
		return activeIND;
	}
	public void setActiveIND(char activeIND) {
		this.activeIND = activeIND;
	}
	public Timestamp getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}
	public Timestamp getModifyDate() {
		return ModifyDate;
	}
	public void setModifyDate(Timestamp modifyDate) {
		ModifyDate = modifyDate;
	}
	public String getUserAddress() {
		return userAddress;
	}
	public void setUserAddress(String userAddress) {
		this.userAddress = userAddress;
	}
	
	
			
		
}
