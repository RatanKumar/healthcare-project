package org.venu.jdbc.domain;

import java.sql.Date;

public class Roles {
 private String roleName;;
 private int roleNbr;
 private int activeInd;
 private Date createDate;
 private Date modifyDate;
 
 
public String getRoleName() {
	return roleName;
}
public void setRoleName(String roleName) {
	this.roleName = roleName;
}
public int getRoleNbr() {
	return roleNbr;
}
public void setRoleNbr(int roleNbr) {
	this.roleNbr = roleNbr;
}
public int getActiveInd() {
	return activeInd;
}
public void setActiveInd(int activeInd) {
	this.activeInd = activeInd;
}
public Date getCreateDate() {
	return createDate;
}
public void setCreateDate(Date createDate) {
	this.createDate = createDate;
}
public Date getModifyDate() {
	return modifyDate;
}
public void setModifyDate(Date modifyDate) {
	this.modifyDate = modifyDate;
}
@Override
public String toString() {
	return "Roles [roleName=" + roleName + ", roleNbr=" + roleNbr
			+ ", activeInd=" + activeInd + ", createDate=" + createDate
			+ ", modifyDate=" + modifyDate + "]";
}
	
	
}
