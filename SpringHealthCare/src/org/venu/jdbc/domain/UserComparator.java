package org.venu.jdbc.domain;

import java.util.Comparator;

/**
 * This class is also useful for sorting here no need of altering User class.so we can write any implementation
 * class then sort.
 * @author venugopal
 *
 */
public class UserComparator implements Comparator<User>{

	public int compare(User o1, User o2) {
		// TODO Auto-generated method stub
		int x=o1.getUserEmail().compareTo(o2.getUserEmail());
		
		return x;
	}
	
}