package org.venu.jdbc.domain;



public  class User  implements Comparable<User>{
  private int userId;
  private String userName;
  private String userPassword;
  private String userEmail;
   public User(int id, String name, String password,String email )
    {
	super();
	this.userId = id;
	this.userName = name;
	this.userPassword=password;
	this.userEmail=email;
	         }
public User() {
	
}
public int getUserId() {
	return userId;
}
public void setUserId(int userId) {
	this.userId = userId;
}
public String getUserName() {
	return userName;
}
public void setUserName(String userName) {
	this.userName = userName;
}
public String getUserPassword() {
	return userPassword;
}
public void setUserPassword(String userPassword) {
	this.userPassword = userPassword;
}

public String getUserEmail() {
	return userEmail;
}
public void setUserEmail(String userEmail) {
	this.userEmail = userEmail;
}
@Override
public String toString() {
	return "User [userId=" + userId + ", userName=" + userName
			+ ", userPassword=" + userPassword + ", userEmail=" + userEmail
			+ "]";
	

	
}

@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + userId;
	result = prime * result + ((userName == null) ? 0 : userName.hashCode());
	result = prime * result
			+ ((userPassword == null) ? 0 : userPassword.hashCode());
	result = prime * result + ((userEmail == null) ? 0 : userEmail.hashCode());
	return result;
}
@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	User other = (User) obj;
	if (userId != other.userId)
		return false;
	if (userName == null) {
		if (other.userName != null)
			return false;
	} else if (!userName.equals(other.userName))
		return false;
	if (userPassword == null) {
		if (other.userPassword != null)
			return false;
	} else if (!userPassword.equals(other.userPassword))
		return false;
	if (userEmail == null) {
		if (other.userEmail != null)
			return false;
	} else if (!userEmail.equals(other.userEmail))
		return false;
	return true;
}
/* This method is used to call collections by Usinng Collections.sor(collection) 
 * and also called by TreeMap and TresSet when we are inserting elements into the these two classes.
 * Here we are sorting elements here based on userName;
 * 
 * */
public int compareTo(User o) {
          
	int i=this.userName.compareTo(o.userName);
	
	return i ;
}


 	
}

