package org.venu.jdbc.domain;

import java.sql.Date;

public class Modules {

	private int moduleNbr;
	private String moduleName;
	private int activeInd;
	private Date createDate;
	private Date modifyDate;
	private int creatUser;

	public int getModuleNbr() {
		return moduleNbr;
	}
	public void setModuleNbr(int moduleNbr) {
		this.moduleNbr = moduleNbr;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public int getActiveInd() {
		return activeInd;
	}
	public void setActiveInd(int activeInd) {
		this.activeInd = activeInd;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	public int getCreatUser() {
		return creatUser;
	}
	public void setCreatUser(int creatUser) {
		this.creatUser = creatUser;
	}
	@Override
	public String toString() {
		return "Modules [moduleNbr=" + moduleNbr + ", moduleName=" + moduleName
				+ ", activeInd=" + activeInd + ", createDate=" + createDate
				+ ", modifyDate=" + modifyDate + ", creatUser=" + creatUser
				+ "]";
	}
	
	
	
}
