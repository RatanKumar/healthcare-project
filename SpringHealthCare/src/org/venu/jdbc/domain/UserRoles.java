package org.venu.jdbc.domain;

public class UserRoles {

	private int urserRoleNbr;
	private int userNbr;
	private int roleNbr;
	
	public int getUrserRoleNbr() {
		return urserRoleNbr;
	}
	public void setUrserRoleNbr(int urserRoleNbr) {
		this.urserRoleNbr = urserRoleNbr;
	}
	public int getUserNbr() {
		return userNbr;
	}
	public void setUserNbr(int userNbr) {
		this.userNbr = userNbr;
	}
	public int getRoleNbr() {
		return roleNbr;
	}
	public void setRoleNbr(int roleNbr) {
		this.roleNbr = roleNbr;
	}

}
