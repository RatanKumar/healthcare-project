package org.venu.jdbc.domain;

import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;

public class UserDetails {
      private int  userDetailsID;
	  private java.sql.Date joindate;
	  private java.sql.Time jointime;
	  private java.sql.Timestamp jointimestamp;
	  private InputStream image_in;
	  private FileOutputStream image_out;
	  private FileReader text_reader;
	  private FileWriter text_writer;
	  private int user_FK;
	  
	  
	  public UserDetails(java.util.Date javadate ,int udid, InputStream image, FileReader text, int user_fk )
	  
	  {
		    this.joindate=new java.sql.Date(javadate.getTime());
			this.jointime=new java.sql.Time(javadate.getTime());
			this.jointimestamp=new java.sql.Timestamp(javadate.getTime());
			this.image_in=image;
			this.text_reader=text;
			this.userDetailsID=udid;
			this.user_FK=user_fk;
	  }


	public int getUserDeatisID() {
		return userDetailsID;
	}


	public void setUserDeatisID(int userDeatisID) {
		this.userDetailsID = userDeatisID;
	}


	public java.sql.Date getJoindate() {
		return joindate;
	}


	public void setJoindate(java.sql.Date joindate) {
		this.joindate = joindate;
	}


	public java.sql.Time getJointime() {
		return jointime;
	}


	public void setJointime(java.sql.Time jointime) {
		this.jointime = jointime;
	}


	public java.sql.Timestamp getJointimestamp() {
		return jointimestamp;
	}


	public void setJointimestamp(java.sql.Timestamp jointimestamp) {
		this.jointimestamp = jointimestamp;
	}


	public InputStream getImage_in() {
		return image_in;
	}


	public void setImage_in(InputStream image_in) {
		this.image_in = image_in;
	}


	public FileOutputStream getImage_out() {
		return image_out;
	}


	public void setImage_out(FileOutputStream image_out) {
		this.image_out = image_out;
	}


	public FileReader getText_reader() {
		return text_reader;
	}


	public void setText_reader(FileReader text_reader) {
		this.text_reader = text_reader;
	}


	public FileWriter getText_writer() {
		return text_writer;
	}


	public void setText_writer(FileWriter text_writer) {
		this.text_writer = text_writer;
	}


	public int getUser_FK() {
		return user_FK;
	}


	public void setUser_FK(int user_FK) {
		this.user_FK = user_FK;
	}
	  
	  
	  
}
