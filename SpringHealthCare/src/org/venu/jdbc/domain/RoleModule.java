package org.venu.jdbc.domain;

import java.sql.Date;

public class RoleModule {
	private int roleModuleNbr;
	private int roleNbr;
	private int moduleNbr;
	private Date createDate;
	private Date modifyDate;
	private char activeInd;
	public int getRoleModuleNbr() {
		return roleModuleNbr;
	}
	public void setRoleModuleNbr(int roleModuleNbr) {
		this.roleModuleNbr = roleModuleNbr;
	}
	public int getRoleNbr() {
		return roleNbr;
	}
	public void setRoleNbr(int roleNbr) {
		this.roleNbr = roleNbr;
	}
	public int getModuleNbr() {
		return moduleNbr;
	}
	public void setModuleNbr(int moduleNbr) {
		this.moduleNbr = moduleNbr;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	public char getActiveInd() {
		return activeInd;
	}
	public void setActiveInd(char activeInd) {
		this.activeInd = activeInd;
	}
	
	
	
	

}
