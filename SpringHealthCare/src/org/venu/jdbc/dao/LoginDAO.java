package org.venu.jdbc.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class LoginDAO     
{
	@Autowired
	private JdbcTemplate jdbcTemplate=null;
	/*public LoginUser authoriseUser(String email, String password) {
		  String sql="select * from STAFF_MASTER where email=? and password=?"; 
          LoginUser login=jdbcTemplate.queryForObject(sql,  new Object[]{email,password} , new RowMapper<LoginUser>() {
			@Override
			public LoginUser mapRow(ResultSet rs, int arg1) throws SQLException {
				LoginUser loginUser=null;
				if(rs.getRow()!=0){
			    loginUser=new LoginUser();
				loginUser.setUserNbr(rs.getInt("usrid"));
				loginUser.setFirstname(rs.getString("first_name"));
				loginUser.setLastname(rs.getString("last_name"));
				loginUser.setEmail(rs.getString("email"));
				loginUser.setPhoneNbr(rs.getString("mobile"));
				loginUser.setStaffType(rs.getInt("staff_type"));
				loginUser.setActiveIND(rs.getString("email").charAt(0));
				}
				return loginUser;
			}
		});
		return login;
	}*/
	
	public int getUserCount(String email, String password){
		 String sql="select count(*) from STAFF_MASTER where email=? and password=?"; 
		int count=jdbcTemplate.queryForInt(sql, email,password);
		return count;
	}
	
	
	
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	
	

}

