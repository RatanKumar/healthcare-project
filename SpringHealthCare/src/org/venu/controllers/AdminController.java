package org.venu.controllers;


import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.venu.jdbc.domain.LoginUser;
import org.venu.service.impl.AdminService;

@Controller
public class AdminController {
	@Autowired
	private AdminService adminService;

	
    private static final Logger LOGGER=LoggerFactory.getLogger(AdminController.class);
	
/*	@RequestMapping(value="/sCreate.htm",method=RequestMethod.GET)
	public ModelAndView getStaffCreate(){
		LOGGER.debug("Inside getHomePage Method:");
		return new ModelAndView("sCreate", "command", new LoginUser());
	}*/
    
	@RequestMapping(value="/sCreate.htm",method=RequestMethod.GET)
	public String getStaffCreate(){
		LOGGER.debug("Inside getStaffCreate Method:");
		return "sCreate";
	}
	
	@RequestMapping(value="/sUpdate.htm", method=RequestMethod.GET)
	public String getStaffUpdate() {
		
		return "sUpdate";
	}

	@RequestMapping(value="/sView.htm", method=RequestMethod.GET)
	public String getStaffView() {
		
		return "sView";
	}
	@RequestMapping(value="/staffAdd.htm",method=RequestMethod.POST)
	public String staffCreate(@ModelAttribute("command") LoginUser loginUser,HttpSession httpSession){
		
		System.out.println("First Name :"+loginUser.getFirstname());
		
		LOGGER.debug("Inside getHomePage Method:");
		return "sCreateSuc";
	}
	
	@RequestMapping(value="/faculty.htm",method=RequestMethod.GET)
	public String getFacultyPage(){
		LOGGER.debug("Inside getHomePage Method:");
		return "faculty";
	}
	
	@RequestMapping(value="/parents.htm",method=RequestMethod.GET)
	public String getParentsPage(){
		LOGGER.debug("Inside getParentsPage Method:");
		return "parents";
	}

	
	@RequestMapping(value="/admin.htm",method=RequestMethod.GET)
	public String getAdminPage(){
		LOGGER.debug("Inside getHomePage Method:");
		return "admin";
	}

	@RequestMapping(value="/staff.html",method=RequestMethod.GET)
	public String getStaffPage(){
		LOGGER.debug("Inside getHomePage Method:");
		return "staff";
	}
	
	
	public AdminService getAdminService() {
		return adminService;
	}

	public void setAdminService(AdminService adminService) {
		this.adminService = adminService;
	}
	
}
