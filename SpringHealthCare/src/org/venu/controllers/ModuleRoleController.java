package org.venu.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.venu.commands.ModuleCommand;
import org.venu.commands.OptionList;
import org.venu.commands.RoleCommand;
import org.venu.jdbc.domain.Modules;
import org.venu.service.impl.ModuleService;
import org.venu.service.impl.RolesService;

@Controller
public class ModuleRoleController{
	
	private ModuleService moduleService=null;
	private RolesService  rolesService=null;
	
	private static final Logger LOGGER=LoggerFactory.getLogger(ModuleRoleController.class);
	
	
	@RequestMapping("/module.htm")
	public ModelAndView getModulePage(){
		LOGGER.info("getModulePage() Method");
	   ModuleCommand mc=	new ModuleCommand();
  
      return  new ModelAndView("admin/module", "modulecommand",mc )  ; 
     }
	
	@RequestMapping("/role.htm")
	public ModelAndView getRolePage(){
		LOGGER.info("getModulePage() Method");
		return   new ModelAndView("admin/role", "rolecommand", new RoleCommand())  ; 
      }
	
  
	
	@RequestMapping(value="/createmodule",method=RequestMethod.POST)
	public String createModule(@ModelAttribute("mc") ModuleCommand mc ,BindingResult result){
		
		return "redirect:module.htm";
		
	}
	
	@RequestMapping(value="/createrole",method=RequestMethod.POST)
     public String createNewModule(@ModelAttribute("role")RoleCommand role,BindingResult result){
    	 
		
    	 return "redirect:role.htm";
     }

		
	@ModelAttribute("screens")
	public List<String> populateScreens(){
		
		List<String> screens=new ArrayList<String>();
		screens.add("Create");
		screens.add("Edit");
		screens.add("Delete");
		
		return screens;
		
	}
	
	@ModelAttribute("modulelist")
	public List<OptionList> populateModules(){
		List<Modules> modules=null;
		
		List<OptionList> modulelist=new ArrayList<OptionList>();
		for (Modules modules2 : modules) {
			
			OptionList option=new OptionList();
			option.setLabel(modules2.getModuleName());
			option.setValue(modules2.getModuleNbr());
			modulelist.add(option);
		}
		
		return modulelist;
	}
	
	
	
	
	
	
	
	public ModuleService getModuleService() {
		return moduleService;
	}
	public void setModuleService(ModuleService moduleService) {
		this.moduleService = moduleService;
	}
	public RolesService getRolesService() {
		return rolesService;
	}
	public void setRolesService(RolesService rolesService) {
		this.rolesService = rolesService;
	}
	public static Logger getLogger() {
		return LOGGER;
	}

	
	
}
