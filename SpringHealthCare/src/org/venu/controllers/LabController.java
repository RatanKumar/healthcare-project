package org.venu.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LabController {
	private static final Logger LOGGER=LoggerFactory.getLogger(LabController.class);

	@RequestMapping(value="/labsearch.htm",method=RequestMethod.GET)
	public String labSearch(){
		LOGGER.debug("Inside getHomePage Method:");
		return "labsearch";
	}

	@RequestMapping(value="/samplecollection.htm",method=RequestMethod.GET)
	public String SampleCollection(){
		LOGGER.debug("Inside getHomePage Method:");
		return "samplecollect";
	}

	@RequestMapping(value="/resultsentry.htm",method=RequestMethod.GET)
	public String resultsEntry(){
		LOGGER.debug("Inside getHomePage Method:");
		return "resultsentry";
	}

	@RequestMapping(value="/resultsverify.htm",method=RequestMethod.GET)
	public String resultsVerify(){
		LOGGER.debug("Inside getHomePage Method:");
		return "resultsverify";
	}

	@RequestMapping(value="/reportgen.htm",method=RequestMethod.GET)
	public String getAdminPage(){
		LOGGER.debug("Inside getHomePage Method:");
		return "reportgen";
	}

	@RequestMapping(value="/reportdispatch.htm",method=RequestMethod.GET)
	public String getStaffPage(){
		LOGGER.debug("Inside getHomePage Method:");
		return "reportdispatch";
	}
	
	
	
}
