package org.venu.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ContactUsController {
	
	private static final Logger LOGGER=LoggerFactory.getLogger(ContactUsController.class);
	
	@RequestMapping(value="/contactus.htm",method=RequestMethod.GET)
	public String getContactUsPage(){
		LOGGER.debug("Inside getContactUsPage Method:");
		return "contactus";
	}

}
