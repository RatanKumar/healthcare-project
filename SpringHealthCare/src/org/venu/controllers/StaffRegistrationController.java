package org.venu.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class StaffRegistrationController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(StaffRegistrationController.class);
	
	@RequestMapping(value="/staffregister.html",method=RequestMethod.GET)
	public String staffRegistration(){
		LOGGER.debug("Inside staffRegistration Method:");
		return "staff/register";
	}
	
	@RequestMapping(value="/staffupadate.html",method=RequestMethod.GET)
	public String staffUpdate(){
		LOGGER.debug("Inside staffUpdate Method:");
		return "staff/update";
	}
	
	@RequestMapping(value="/staffview.html",method=RequestMethod.GET)
	public String staffView(){
		LOGGER.debug("Inside staffView Method:");
		return "staff/view";
	}

}
