package org.venu.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RegistrationController {
	private static final Logger LOGGER=LoggerFactory.getLogger(RegistrationController.class);

	@RequestMapping(value="/registration.htm",method=RequestMethod.GET)
	public String registration(){
		LOGGER.debug("Inside getHomePage Method:");
		return "registration";
	}

	@RequestMapping(value="/rcreate.htm",method=RequestMethod.GET)
	public String SampleCollection(){
		LOGGER.debug("Inside getHomePage Method:");
		return "rcreate";
	}

	@RequestMapping(value="/rupdate.htm",method=RequestMethod.GET)
	public String resultsEntry(){
		LOGGER.debug("Inside getHomePage Method:");
		return "rupdate";
	}

	@RequestMapping(value="/rcancel.htm",method=RequestMethod.GET)
	public String resultsVerify(){
		LOGGER.debug("Inside getHomePage Method:");
		return "rcancel";
	}
	
/*	@RequestMapping(value="/staffregister.htm",method=RequestMethod.GET)
	public String staffRegistration(){
		LOGGER.debug("Inside staffRegistration Method:");
		return "staff/register";
	}
	
	@RequestMapping(value="/staffupadate.htm",method=RequestMethod.GET)
	public String staffUpdate(){
		LOGGER.debug("Inside staffUpdate Method:");
		return "staff/update";
	}
	
	@RequestMapping(value="/staffview.htm",method=RequestMethod.GET)
	public String staffView(){
		LOGGER.debug("Inside staffView Method:");
		return "staff/view";
	}*/
	
}
