package org.venu.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.venu.commands.ValidXML;
import org.venu.service.IUserService;

@Controller

public class LoginController {

	private static final Logger LOGGER=LoggerFactory.getLogger(LoginController.class);
	private IUserService userService;

	@RequestMapping(value="/login.htm", method=RequestMethod.GET , headers="accept=*/*")
	public @ResponseBody  ValidXML loginAccess(ModelMap model,HttpServletRequest req,HttpServletResponse res){
		LOGGER.info("request is in Logincontroller-login starting point..");
		String email=req.getParameter("uname");
		String password=req.getParameter("pass");
		ValidXML xmlObject=new ValidXML();
		
		if(email.equals("admin")&&password.equals("admin") )   
			xmlObject.setValid("true");
		else
			xmlObject.setValid("false");
		return xmlObject;
	}

	@RequestMapping(value="/body.htm", method=RequestMethod.GET )
	public String getBody(ModelMap model,HttpServletRequest req,HttpServletResponse res){

		LOGGER.info("request is in Logincontroller-body starting point");

		/*return "admin/adminhome";*/
		return "admin/home";

	}


	@RequestMapping(value="/accordmenu.htm",method=RequestMethod.GET)
	public String getAccord(){
		LOGGER.info("request is in getAccord starting point");
		return "admin/menu";
	}

	public IUserService getUserService() {
		return userService;
	}

	public void setUserService(IUserService userService) {
		this.userService = userService;
	}






}
