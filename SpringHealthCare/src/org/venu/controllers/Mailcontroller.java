package org.venu.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class Mailcontroller {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Mailcontroller.class);

	@RequestMapping(value="/mail.htm", method=RequestMethod.GET)
    public String getAdminMenu(){
		System.out.println("Hi from Mailcontroller-Admin Page");
		LOGGER.debug("Inside getAdminMenu Method");
		return "admin/mail";
		//return "mail";
	}
	
}
