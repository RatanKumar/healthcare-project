package org.venu.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.venu.commands.OptionList;
import org.venu.commands.UserCommand;
import org.venu.jdbc.domain.Roles;
import org.venu.service.IUserService;
import org.venu.service.impl.RolesService;
@Controller
public class UserController {
	
	private static final Logger LOGGER=LoggerFactory.getLogger(UserController.class) ;
	
	private IUserService userService=null;
	private RolesService rolesService=null;

	@RequestMapping("/user.htm")
	public ModelAndView getUserPage(){

		LOGGER.info("getUserPage() Method");

		return new ModelAndView("admin/user", "usercommand", new UserCommand())  ;  
	}

	@RequestMapping("/createuser.htm")
	public String newUser(@ModelAttribute("user")UserCommand user,BindingResult result){

		return "redirect:user.htm";

	}

	//   
	@ModelAttribute("books")
	public List<String> getBookList(){

		List<String> books=new ArrayList<String>();
		books.add("You can win ");
		books.add("How to build a better Vocabulary");
		books.add("All about Words");
		return books;
	}

	@ModelAttribute("hobbies")
	public List<String> getHobbiesList(){

		List<String> hobbies=new ArrayList<String>();
		hobbies.add("Reading");
		hobbies.add("Programming");
		hobbies.add("Sleeping");
		return hobbies;
	}


	@ModelAttribute("roles")
	public List<OptionList> getRolesList(){

		List<Roles> roles=null;
		List<OptionList> roleslist=new ArrayList<OptionList>();

		for (Roles roles2 : roles) {
			OptionList optionList=new OptionList();
			optionList.setLabel(roles2.getRoleName());
			optionList.setValue(roles2.getRoleNbr());
			roleslist.add(optionList);
		}

		return roleslist;
	}



	public IUserService getUserService() {
		return userService;
	}


	public void setUserService(IUserService userService) {
		this.userService = userService;
	}

	public RolesService getRolesService() {
		return rolesService;
	}

	public void setRolesService(RolesService rolesService) {
		this.rolesService = rolesService;
	}






}
