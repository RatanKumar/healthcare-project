package org.venu.hib.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
@Entity
@Table(name = "module", catalog = "hospital", uniqueConstraints = {
		@UniqueConstraint(columnNames = "moduleId"),
		})
public class Module {
	
	private long moduleId;
	private String moduleName;
	private String moduleUrl;
	private String moduleDesc;
	private List<ModulePages> modulePages;
	private Date  createDate;
	private Date modifyDate;
	private char activeIndicator;
	private Staff createUser;
	private Staff modifyUser;
	
	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	@Column(name = "moduleId", unique = true, nullable = false,updatable=false)
	public long getModuleId() {
		return moduleId;
	}
	public void setModuleId(long moduleId) {
		this.moduleId = moduleId;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getModuleUrl() {
		return moduleUrl;
	}
	public void setModuleUrl(String moduleUrl) {
		this.moduleUrl = moduleUrl;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable=false)
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	public Date getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	public char getActiveIndicator() {
		return activeIndicator;
	}
	public void setActiveIndicator(char activeIndicator) {
		this.activeIndicator = activeIndicator;
	}
	public String getModuleDesc() {
		return moduleDesc;
	}
	public void setModuleDesc(String moduleDesc) {
		this.moduleDesc = moduleDesc;
	}
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "module",orphanRemoval=true)
	public List<ModulePages> getModulePages() {
		return modulePages;
	}
	public void setModulePages(List<ModulePages> modulePages) {
		this.modulePages = modulePages;
	}
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
	public Staff getCreateUser() {
		return createUser;
	}
	public void setCreateUser(Staff createUser) {
		this.createUser = createUser;
	}
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
	public Staff getModifyUser() {
		return modifyUser;
	}
	public void setModifyUser(Staff modifyUser) {
		this.modifyUser = modifyUser;
	}
	
	
}
