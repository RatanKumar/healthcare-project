package org.venu.hib.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.CollectionOfElements;

@Entity
@Table(name = "STAFF", catalog = "hospital", 
uniqueConstraints = @UniqueConstraint(columnNames = "staffId"))
public class Staff {
	
	private long staffId;
	private String firstName;
	private String lastName;
	private String imageUrl;
	private String email;
	private Date dob;
	private Date jod;
	private String phoneNumber;
	private Role role;
	@Embedded
	private StaffAddress staffAddress;
	private String[] hobbies;
	private Date createDate;
	private Date modifyDate;
	private char activeIndicator;
	private Staff createUser;
	private Staff modifyUser;

	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	@Column(name = "staffId", unique = true, nullable = false,updatable=false)
	public long getStaffId() {
		return staffId;
	}

	public void setStaffId(long staffId) {
		this.staffId = staffId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Temporal(TemporalType.DATE)
	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}
	@Temporal(TemporalType.DATE)
	public Date getJod() {
		return jod;
	}

	public void setJod(Date jod) {
		this.jod = jod;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.DETACH)
	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public StaffAddress getStaffAddress() {
		return staffAddress;
	}

	public void setStaffAddress(StaffAddress staffAddress) {
		this.staffAddress = staffAddress;
	}

   @SuppressWarnings("deprecation")
   @CollectionOfElements
   @JoinTable(name="STAFF_HOBBIES")
   @OrderColumn
	public String[] getHobbies() {
		return hobbies;
	}

	public void setHobbies(String[] hobbies) {
		this.hobbies = hobbies;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable=false)
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public char getActiveIndicator() {
		return activeIndicator;
	}

	public void setActiveIndicator(char activeIndicator) {
		this.activeIndicator = activeIndicator;
	}
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
	public Staff getCreateUser() {
		return createUser;
	}

	public void setCreateUser(Staff createUser) {
		this.createUser = createUser;
	}
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
	public Staff getModifyUser() {
		return modifyUser;
	}

	public void setModifyUser(Staff modifyUser) {
		this.modifyUser = modifyUser;
	}

}
