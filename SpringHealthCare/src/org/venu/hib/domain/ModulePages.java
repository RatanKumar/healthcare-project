package org.venu.hib.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
@Entity
@Table(name = "module_pages", catalog = "hospital", 
uniqueConstraints = @UniqueConstraint(columnNames = "pageId"))
public class ModulePages {
	
	private long pageId;
	private String pageName;
	private String pageUrl;
	private String pageDesc;
	private Module module;
	private Date  createDate;
	private Date modifyDate;
	private char activeIndicator;
	private Staff createUser;
	private Staff modifyUser;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pageId", unique = true, nullable = false)
	public long getPageId() {
		return pageId;
	}
	public void setPageId(long pageId) {
		this.pageId = pageId;
	}
	public String getPageName() {
		return pageName;
	}
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	public String getPageUrl() {
		return pageUrl;
	}
	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}
	public String getPageDesc() {
		return pageDesc;
	}
	public void setPageDesc(String pageDesc) {
		this.pageDesc = pageDesc;
	}
	@ManyToOne(fetch = FetchType.LAZY,cascade=CascadeType.DETACH)
	@JoinColumn(name = "moduleId", nullable = false)
	public Module getModule() {
		return module;
	}
	public void setModule(Module module) {
		this.module = module;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable=false)
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	@Temporal(TemporalType.TIMESTAMP)
	public Date getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	public char getActiveIndicator() {
		return activeIndicator;
	}
	public void setActiveIndicator(char activeIndicator) {
		this.activeIndicator = activeIndicator;
	}
	
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
	public Staff getCreateUser() {
		return createUser;
	}
	public void setCreateUser(Staff createUser) {
		this.createUser = createUser;
	}
	@OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.DETACH)
	public Staff getModifyUser() {
		return modifyUser;
	}
	public void setModifyUser(Staff modifyUser) {
		this.modifyUser = modifyUser;
	}

}
