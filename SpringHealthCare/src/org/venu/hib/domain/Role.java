package org.venu.hib.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
@Entity
@Table(name = "ROLE", catalog = "hospital", uniqueConstraints = {
		@UniqueConstraint(columnNames = "roleId"),
		})
public class Role {

	//rolename desc readate moddaata
	private long roleId;
	private String roleName;
	private String description;
	private Date creatDate;
	private Date modifyDate;
	private Staff createUser;
	private Staff modifyUser;
	
	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	@Column(name = "roleId", unique = true, nullable = false,updatable=false)
	public long getRoleId() {
		return roleId;
	}
	public void setRoleId(long roleId) {
		this.roleId = roleId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable=false)
	public Date getCreatDate() {
		return creatDate;
	}
	public void setCreatDate(Date creatDate) {
		this.creatDate = creatDate;
	}
	@Temporal(TemporalType.TIMESTAMP)
	public Date getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	
	@OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.DETACH)
	public Staff getCreateUser() {
		return createUser;
	}
	public void setCreateUser(Staff createUser) {
		this.createUser = createUser;
	}
	@OneToOne(fetch = FetchType.LAZY,  cascade = CascadeType.DETACH)
	public Staff getModifyUser() {
		return modifyUser;
	}
	public void setModifyUser(Staff modifyUser) {
		this.modifyUser = modifyUser;
	}
}
