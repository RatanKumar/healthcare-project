package org.venu.hib.domain;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "Role_Modules", catalog = "hospital", 
uniqueConstraints = @UniqueConstraint(columnNames = "roleModuleId"))
public class RoleModules {

	private long roleModuleId;
	private Role role;
	private Set<Module> modules;
	private String modulePage;
	private Date createDate;
	private Date modifyDate;
	private char activeIndicator;
	private Staff createUser;
	private Staff modifyUser;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "roleModuleId", unique = true, nullable = false)
	public long getRoleModuleId() {
		return roleModuleId;
	}
	public void setRoleModuleId(long roleModuleId) {
		this.roleModuleId = roleModuleId;
	}
	@OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.DETACH)
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	@OneToMany(fetch = FetchType.EAGER, orphanRemoval=true,cascade=CascadeType.ALL)
	public Set<Module> getModules() {
		return modules;
	}
	public void setModules(Set<Module> modules) {
		this.modules = modules;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable=false)
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	@Temporal(TemporalType.TIMESTAMP)
	public Date getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	public char getActiveIndicator() {
		return activeIndicator;
	}
	public void setActiveIndicator(char activeIndicator) {
		this.activeIndicator = activeIndicator;
	}
	@OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.DETACH)
	public Staff getCreateUser() {
		return createUser;
	}
	public void setCreateUser(Staff createUser) {
		this.createUser = createUser;
	}
	@OneToOne(fetch = FetchType.LAZY,  cascade = CascadeType.DETACH)
	public Staff getModifyUser() {
		return modifyUser;
	}
	public void setModifyUser(Staff modifyUser) {
		this.modifyUser = modifyUser;
	}
	public String getModulePage() {
		return modulePage;
	}
	public void setModulePage(String modulePage) {
		this.modulePage = modulePage;
	}
	
	
}
