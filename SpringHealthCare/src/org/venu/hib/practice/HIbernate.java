package org.venu.hib.practice;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.venu.hib.domain.Module;
import org.venu.hib.domain.ModulePages;
import org.venu.hib.domain.Role;
import org.venu.hib.domain.RoleModules;
import org.venu.hib.domain.Staff;
import org.venu.hib.domain.StaffAddress;
public class HIbernate {
 		/**
		 * @param args
		 */
		public static void main(String[] args) {
			// TODO Auto-generated method stub

			Configuration config=new Configuration();
			/**
			 * when we use this function it reads class metadata from annotations for mapping.
			 * So Configure here every class annotated as Entity
			 * 
			 * Here you can also add mapped file test it
			 */
			config.configure("org/venu/hib/practice/hibernate.cfg.xml");
		//	config.addPackage("org.his.hib.domain");
			config.addAnnotatedClass(org.venu.hib.domain.Module.class);
			config.addAnnotatedClass(org.venu.hib.domain.ModulePages.class);
			config.addAnnotatedClass(org.venu.hib.domain.Role.class);
			config.addAnnotatedClass(org.venu.hib.domain.Staff.class);
			config.addAnnotatedClass(org.venu.hib.domain.StaffAddress.class);
			config.addAnnotatedClass(org.venu.hib.domain.RoleModules.class);

			/*SchemaExport schemaExport	=new SchemaExport(config);
			schemaExport.create(true, true);
			*/
		/*	SchemaUpdate schemaUpdate=new SchemaUpdate(config);
			schemaUpdate.execute(true, true);
			*/
		    SessionFactory sessionFactory=config.buildSessionFactory();
			Session session=null;
			Transaction transaction=null;
			try {
				session=sessionFactory.openSession();
				transaction=session.beginTransaction();
				
				/*
				Creating role code
				Staff staff=new Staff();
				staff.setStaffId(1);
		    	Role role=createRole("Reception", "Reception", staff);
				*/
				/*Staff staff=new Staff();
				staff.setStaffId(1);
			    Module module=	createModule("OP Billing Master", "/opbillmaster.html", staff);*/
				/*Staff staff=new Staff();
				staff.setStaffId(1);
				Module module=new  Module();
				module.setModuleId(2);
				ModulePages pages=createPage("Report Generation", "/reportgen.html", "Lab Tet Report Generation", staff, module);*/
		
			/*	Staff staff=new Staff();
				staff.setStaffId(1);
				Role role=new Role();
				role.setRoleId(1);
				
				Module module1=new  Module();
				module1.setModuleId(1);
				Module module2=new  Module();
				module2.setModuleId(2);
				Module module3=new  Module();
				module3.setModuleId(3);
				Module module4=new  Module();
				module3.setModuleId(4);
				
		    	Set<Module> modules=	new HashSet<Module>();
		    	modules.add(module1);
		    	modules.add(module2);
		    	modules.add(module3);
		    	modules.add(module4);
		    	
				
				RoleModules roleModules=new RoleModules();
				roleModules.setModulePage(null);
				roleModules.setModules(modules);
				roleModules.setRole(role);
				
				
				roleModules.setCreateDate(new Date());
				roleModules.setModifyUser(staff);
				roleModules.setCreateUser(staff);
				roleModules.setModifyDate(new Date());
				roleModules.setActiveIndicator('Y');
				*/
		/*		 
		 * 				String q="from Staff where email =:email  and lastName=:lastName";

				Query query =session.createQuery(q);
				query.setParameter("email", "venkat@gmail.com");
				query.setParameter("lastName", "Reddy");
			List<Staff> list=	query.list();
				
				for (Staff staff2 : list) {
					System.out.println(staff2.getEmail());
					System.out.println(staff2.getLastName());
					System.out.println(staff2.getRole().getRoleName());
					
				}*/
				
				String q="from RoleModules where role.roleId =:roleId";
 
				Query query =session.createQuery(q);
				query.setParameter("roleId",new Long(1));
			List<RoleModules> list=	(List<RoleModules>)query.list();
				
	/*		
			Staff staff=	(Staff)session.get(Staff.class, new Long(1));
			Role role=new Role();
			role.setRoleId(1);
			staff.setRole(role);
				
				
				
				session.saveOrUpdate(staff);
				*/
				transaction.commit();
				session.close();
				
				for (RoleModules rm : list) {
					  for (Module module : rm.getModules()) {
						  for (ModulePages mp : module.getModulePages()) {
							  System.out.println(mp.getPageName());
							
						}
						
					}
				}
				
				
				
				
				
			} catch (Exception exception) {
				transaction.rollback();
				exception.printStackTrace();
				
			}finally{
				//session.close();
			}
		
			System.out.println("Table Schema exported successuflly");
			
			
			
		}
		
		
		private static Role createRole(String rolename,String roledesc, Staff  staff){

			Role role=new Role();
			role.setRoleName(roledesc);
			role.setDescription(roledesc);
			role.setCreatDate(new Date());
			role.setModifyDate(new Date());
			
			role.setCreateUser(staff);
			role.setModifyUser(staff);
			return role;
		}
		
		private static Staff createUser(){
			Staff staff=new Staff();
			
			staff.setFirstName("Venkat");
			staff.setLastName("Reddy");
			staff.setImageUrl("/venkat.jpg");
			staff.setEmail("venkat@gmail.com");
			staff.setDob(new Date());
			staff.setJod(new Date());
			staff.setPhoneNumber("9177773087");
			staff.setRole(null);
			
			StaffAddress staffAddress=new StaffAddress();
			staffAddress.setCity("Hyderabad");
			staffAddress.setCountry("India");
			
			
			staff.setStaffAddress(staffAddress);
			String [] hobbies={"Reading","Writing"};
			staff.setHobbies(hobbies);
			staff.setActiveIndicator('y');
			staff.setCreateUser(null);
			staff.setModifyUser(null);	
			
	       return staff;
		}
		
		private static Module createModule(String moduleName,String moduleUrl, Staff  staff){
			Module module=new Module();
			module.setModuleName(moduleName);
			module.setModulePages(null);
			module.setModuleUrl(moduleUrl);
			module.setCreateDate(new Date());
			module.setModifyUser(staff);
			module.setCreateUser(staff);
			module.setModifyDate(new Date());
			module.setActiveIndicator('Y');
			
			return module;
			
		}
		private static ModulePages createPage(String pageName,String pageUrl,String pageDesc ,Staff  staff,Module module){
			ModulePages pages=new ModulePages();
			pages.setPageName(pageName);
			pages.setPageUrl(pageUrl);
			pages.setPageDesc(pageDesc);
		
			pages.setModule(module);
			
			pages.setCreateUser(staff);
			pages.setCreateDate(new Date());
			pages.setModifyUser(staff);
			pages.setModifyDate(new Date());
			pages.setActiveIndicator('Y');
			
			return  pages;
			
		}
		
		
		private static RoleModules creatRoleModules(Staff  staff,Module module,Role role){
			RoleModules roleModules=new RoleModules();
			roleModules.setModulePage(null);
			roleModules.setModules(null);
			roleModules.setRole(role);
			
			
			roleModules.setCreateDate(new Date());
			roleModules.setModifyUser(staff);
			roleModules.setModifyDate(new Date());
			roleModules.setActiveIndicator('Y');
			
			return roleModules;
		}
		
		
		
}

