create table hospital.ROLE (roleId bigint not null auto_increment unique, creatDate datetime, description varchar(255), modifyDate datetime, roleName varchar(255), createUser_staffId bigint, modifyUser_staffId bigint, primary key (roleId), unique (roleId))

show tables
select * from role
select * from staff
select * from role_modules
select * from role_modules_module
select * from module
select * from module_pagespp

create table hospital.ROLE (roleId bigint not null auto_increment unique, creatDate datetime, description varchar(255), modifyDate datetime, roleName varchar(255), createUser_staffId bigint, modifyUser_staffId bigint, primary key (roleId), unique (roleId))
create table hospital.STAFF (staffId bigint not null auto_increment unique, activeIndicator char(1) not null, createDate datetime, dob date, email varchar(255), firstName varchar(255), hobbies tinyblob, imageUrl varchar(255), jod date, lastName varchar(255), modifyDate datetime, phoneNumber varchar(255), city varchar(255), contry varchar(255), createUser_staffId bigint, modifyUser_staffId bigint, role_roleId bigint, primary key (staffId), unique (staffId))
create table hospital.module (moduleId bigint not null auto_increment unique, activeIndicator char(1) not null, createDate datetime, modifyDate datetime, moduleDesc varchar(255), moduleName varchar(255), moduleUrl varchar(255), createUser_staffId bigint, modifyUser_staffId bigint, primary key (moduleId), unique (moduleId))
create table hospital.module_pages (pageId bigint not null auto_increment unique, activeIndicator char(1) not null, createDate datetime, modifyDate datetime, pageDesc varchar(255), pageName varchar(255), pageUrl varchar(255), createUser_staffId bigint, modifyUser_staffId bigint, moduleId bigint not null, primary key (pageId), unique (pageId))
alter table hospital.ROLE add index FK267876F9459252 (modifyUser_staffId), add constraint FK267876F9459252 foreign key (modifyUser_staffId) references hospital.STAFF (staffId)
alter table hospital.ROLE add index FK26787682B6794 (createUser_staffId), add constraint FK26787682B6794 foreign key (createUser_staffId) references hospital.STAFF (staffId)
alter table hospital.STAFF add index FK4B8CAC0F9459252 (modifyUser_staffId), add constraint FK4B8CAC0F9459252 foreign key (modifyUser_staffId) references hospital.STAFF (staffId)
alter table hospital.STAFF add index FK4B8CAC082B6794 (createUser_staffId), add constraint FK4B8CAC082B6794 foreign key (createUser_staffId) references hospital.STAFF (staffId)
alter table hospital.STAFF add index FK4B8CAC07CB77C1F (role_roleId), add constraint FK4B8CAC07CB77C1F foreign key (role_roleId) references hospital.ROLE (roleId)
alter table hospital.module add index FKC04BA66CF9459252 (modifyUser_staffId), add constraint FKC04BA66CF9459252 foreign key (modifyUser_staffId) references hospital.STAFF (staffId)
alter table hospital.module add index FKC04BA66C82B6794 (createUser_staffId), add constraint FKC04BA66C82B6794 foreign key (createUser_staffId) references hospital.STAFF (staffId)
alter table hospital.module_pages add index FK600C7831F9459252 (modifyUser_staffId), add constraint FK600C7831F9459252 foreign key (modifyUser_staffId) references hospital.STAFF (staffId)
alter table hospital.module_pages add index FK600C783182B6794 (createUser_staffId), add constraint FK600C783182B6794 foreign key (createUser_staffId) references hospital.STAFF (staffId)
alter table hospital.module_pages add index FK600C78314CD9C6E2 (moduleId), add constraint FK600C78314CD9C6E2 foreign key (moduleId) references hospital.module (moduleId)


select * from staff
select * from role
select * from module
select * from module_pages



insert into hospital.STAFF (activeIndicator, createDate, createUser_staffId, dob, email, firstName, hobbies, imageUrl, jod, lastName, modifyDate, modifyUser_staffId, phoneNumber, role_roleId, city, contry) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)


Hibernate: select staff0_.staffId as staffId3_, staff0_.activeIndicator as activeIn2_3_, staff0_.createDate as createDate3_, staff0_.createUser_staffId as createUser14_3_, staff0_.dob as dob3_, staff0_.email as email3_, staff0_.firstName as firstName3_, staff0_.imageUrl as imageUrl3_, staff0_.jod as jod3_, staff0_.lastName as lastName3_, staff0_.modifyDate as modifyDate3_, staff0_.modifyUser_staffId as modifyUser15_3_, staff0_.phoneNumber as phoneNu11_3_, staff0_.role_roleId as role16_3_, staff0_.city as city3_, staff0_.country as country3_ from hospital.STAFF staff0_ where staff0_.email=? and staff0_.lastName=?
Hibernate: select hobbies0_.STAFF_staffId as STAFF1_3_0_, hobbies0_.element as element0_, hobbies0_.hobbies_ORDER as hobbies3_0_ from STAFF_HOBBIES hobbies0_ where hobbies0_.STAFF_staffId=?







select * from staff