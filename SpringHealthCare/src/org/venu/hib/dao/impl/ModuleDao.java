package org.venu.hib.dao.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.venu.hib.dao.IModuleDao;
import org.venu.jdbc.domain.Modules;

public class ModuleDao implements IModuleDao {

	private HibernateTemplate hibernateTemplate = null;

	public Modules crateNewModule(Modules modules) {

		hibernateTemplate.save(modules);

		return modules;
	}

	public List<Modules> getModulesList() {

		DetachedCriteria criteria = DetachedCriteria.forClass(Modules.class);

		@SuppressWarnings("unchecked")
		List<Modules> modules = (List<Modules>) hibernateTemplate
				.findByCriteria(criteria);

		return modules;

	}

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

}
