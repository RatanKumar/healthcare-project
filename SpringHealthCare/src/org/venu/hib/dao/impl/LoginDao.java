package org.venu.hib.dao.impl;

import java.util.List;

import org.venu.hib.dao.ILoginDao;
import org.venu.hib.domain.RoleModules;
import org.venu.hib.domain.Staff;


public class LoginDao extends GenericDao implements ILoginDao {

	@SuppressWarnings("unchecked")
	@Override
	public Staff getStfaff(String userName, String passWord) {
		String query="from Staff where email =:email  and lastName=:lastName";
	 List<Staff> staff=hibernateTemplate.findByNamedParam(query, new String[]{"email","lastName"}, new Object[]{userName,passWord});
	if(staff.size()==0) return null;
	else
	 return staff.get(0);
	}

	@Override
	public List<RoleModules> getAuthourisePages(long roleId) {
		
		
		String query="from RoleModules where role.roleId =?";
		List<RoleModules> list=hibernateTemplate.find(query, new Object[]{roleId}); 
		
		return list;
	}
	
	
	
	
	
}
