package org.venu.hib.dao.impl;

import org.springframework.orm.hibernate3.HibernateTemplate;


public class GenericDao   {
	
	protected HibernateTemplate hibernateTemplate;

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

}
