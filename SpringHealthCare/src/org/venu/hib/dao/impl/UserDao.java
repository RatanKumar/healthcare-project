package org.venu.hib.dao.impl;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.venu.hib.dao.IUserDao;
import org.venu.jdbc.domain.User;

public class UserDao extends GenericDao implements IUserDao {

	public User creatNewUser(User user){
		hibernateTemplate.save(user);
		return user;
	}
	
	@Override
	public User getUser(int primaryKey){
        User user=hibernateTemplate.get(User.class, primaryKey );
		return user;
	}

	public User authorizeUser(String email, String password){
		User user=new User();
		
		SessionFactory sessionFactory = hibernateTemplate.getSessionFactory();
		Session session=  sessionFactory.openSession();
		Transaction tx=session.beginTransaction();
		
		Query query=session.createQuery("from User u where u.email=:email and u.firtName=:firtName");
		query.setParameter("email", email);
		query.setParameter("firtName", password);
		@SuppressWarnings("unchecked")
		List<User> ulist= query.list();

		for (User u : ulist) {
			user=u;
		}

		tx.commit();
		session.close();

		return user;
	}












}
