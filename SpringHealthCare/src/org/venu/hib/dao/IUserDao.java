package org.venu.hib.dao;

import org.venu.jdbc.domain.User;


public interface IUserDao {

	User getUser(int primaryKey);

}
