package org.venu.hib.dao;

public interface StaffDao {

	void createUser();
	void deleteUser();
	void updateUser();
	void viewStaffUser();
	
	
}
