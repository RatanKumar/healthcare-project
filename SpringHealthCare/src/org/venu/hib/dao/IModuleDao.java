package org.venu.hib.dao;

import java.util.List;

import org.venu.jdbc.domain.Modules;


public interface IModuleDao {

	Modules crateNewModule(Modules modules);

	List<Modules> getModulesList();
}
