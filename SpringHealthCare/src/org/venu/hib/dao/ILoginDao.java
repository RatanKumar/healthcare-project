package org.venu.hib.dao;

import java.util.List;

import org.venu.hib.domain.RoleModules;
import org.venu.hib.domain.Staff;

public interface ILoginDao {
	public Staff getStfaff(String userName,String passWord);
	public List<RoleModules> getAuthourisePages(long roleId);
}
