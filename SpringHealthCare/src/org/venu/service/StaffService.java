package org.venu.service;

public interface StaffService {
	
	void createUser();
	void updateUser();
	void viewStaffUser();
	void deleteUser();
}
