package org.venu.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionTemplate;
import org.venu.hib.dao.IUserDao;
import org.venu.service.IUserService;


public class UserService implements IUserService{

   private static final Logger LOGGER=LoggerFactory.getLogger(UserService.class);
	
	private IUserDao userDao = null;
    private TransactionTemplate transactionTemplate=null;
	
	
	/*@Override
	public User authorizeUser(final String email,final String password){
		
		LOGGER.info("Method  is executing in Transaction");
		
		User user=transactionTemplate.execute(new TransactionCallback<User>() {
			
			@Override
			public User doInTransaction(TransactionStatus txStatus) {
                User u=null;
				
				try {
					LOGGER.info("Inside Transaction:");
					u=userDao.authorizeUser(email, password);
				} catch (RuntimeException e) {
					txStatus.setRollbackOnly();
					
				}
			return u;
			    }
		     });
		return user;
	}*/
	
	
	public IUserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(IUserDao userDao) {
		this.userDao = userDao;
	}
	public TransactionTemplate getTransactionTemplate() {
		return transactionTemplate;
	}
	public void setTransactionTemplate(TransactionTemplate transactionTemplate) {
		this.transactionTemplate = transactionTemplate;
	}

	
	
	
}
