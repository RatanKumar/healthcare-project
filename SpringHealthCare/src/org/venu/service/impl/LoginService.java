package org.venu.service.impl;

import org.springframework.stereotype.Service;
import org.venu.hib.dao.impl.LoginDao;
import org.venu.hib.domain.Staff;
import org.venu.service.ILoginService;

@Service
public class LoginService implements ILoginService  {
private LoginDao loginDao=null;

public LoginDao getLoginDao() {
	return loginDao;
}

public void setLoginDao(LoginDao loginDao) {
	this.loginDao = loginDao;
}

@Override
public Staff authorizeUser(String usernName, String passWord) {
	// TODO Auto-generated method stub
	return  loginDao.getStfaff(usernName, passWord);
}





}
