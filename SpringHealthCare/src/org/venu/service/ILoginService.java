package org.venu.service;

import org.venu.hib.domain.Staff;


public interface ILoginService {
	public Staff authorizeUser(String usernName,String passWord) ;
}
