package org.venu.actions;

import org.venu.actions.beans.StaffBean;

import com.opensymphony.xwork2.ModelDriven;

public class StaffAction extends BaseAction implements ModelDriven<StaffBean> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	StaffBean staffBean=new StaffBean();
	
	public String createUser() {
		
		System.out.println("staff register page");
		return "success";
	}
	
	public String updateUser() {
		return "success";
	}
	
	public String viewStaffUser(){
		System.out.println("Staff viewpage");
		return "success";
	}

	public String deleteUser() {
		return "success";
	}
	@Override
	public StaffBean getModel() {
		return staffBean;
	}
	
        @Override
		public void validate(){
        	if("mkyong".equals(null)){
    			addActionMessage("You are valid user!");
    		}else{
    			addActionError("I don't know you, dont try to hack me!");
    		}
		}
}
