package org.venu.actions;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.venu.hib.domain.Staff;
import org.venu.service.ILoginService;

public class LoginAction extends BaseAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER=LoggerFactory.getLogger(LoginAction.class);
	private ILoginService loginService=null;
	
	private String userName;
	private String passWord;
	private boolean access;
	
	
	public  String loginAccess(){
		         	LOGGER.info("request is in Logincontroller starting point");
		       Staff staff=loginService.authorizeUser(userName, passWord);
		       if(staff!=null){
		    	   HttpSession httpSession=httpServletRequest.getSession(false);
		    	   httpSession.setAttribute("loginStaff", staff);
		    	   System.out.println(staff.getFirstName());
		    	   this.setAccess(true);
		       }
		         	System.out.println("HI frm venu");
         	return "success";
				
	}

	public ILoginService getLoginService() {
		return loginService;
	}

	public void setLoginService(ILoginService loginService) {
		this.loginService = loginService;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public boolean isAccess() {
		return access;
	}

	public void setAccess(boolean access) {
		this.access = access;
	}
	
	
	
	
}
