package org.venu.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.venu.controllers.AdminController;

public class AdminAction extends BaseAction{

	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER=LoggerFactory.getLogger(AdminController.class);
	
	public String getAdminPage(){
		LOGGER.debug("Inside getHomePage Method:");
		return "admin";
	}

	public String getStaffPage(){
		LOGGER.debug("Inside getHomePage Method:");
		return "staff";
	}
	
	
	
	
	
	
}
