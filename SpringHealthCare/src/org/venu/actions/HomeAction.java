package org.venu.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HomeAction extends BaseAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER=LoggerFactory.getLogger(HomeAction.class);
	
	
	public String getHomePage(){
		LOGGER.debug("Inside getHomePage Method:");
		System.out.println("Hi from Home action");
		return "home";
	}

}
