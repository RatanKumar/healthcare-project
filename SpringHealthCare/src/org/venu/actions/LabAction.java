package org.venu.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.venu.controllers.LabController;

public class LabAction extends BaseAction{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER=LoggerFactory.getLogger(LabController.class);

	public String labSearch(){
		LOGGER.debug("Inside getHomePage Method:");
		return "labsearch";
	}

	public String SampleCollection(){
		LOGGER.debug("Inside getHomePage Method:");
		return "samplecollect";
	}

	public String resultsEntry(){
		LOGGER.debug("Inside getHomePage Method:");
		return "resultsentry";
	}

	public String resultsVerify(){
		LOGGER.debug("Inside getHomePage Method:");
		return "resultsverify";
	}

	public String reportGen(){
		LOGGER.debug("Inside getHomePage Method:");
		return "reportgen";
	}

	public String reportDispatch(){
		LOGGER.debug("Inside getHomePage Method:");
		return "reportdispatch";
	}
	
}
