package org.venu.commands;

import java.util.List;

public class ModuleCommand {

	private String moduleName;
	private String moduleDescription;
	private List<String> screens;
	
	
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getModuleDescription() {
		return moduleDescription;
	}
	public void setModuleDescription(String moduleDescription) {
		this.moduleDescription = moduleDescription;
	}
	public List<String> getScreens() {
				
		return screens;
	}
	public void setScreens(List<String> screens) {
		this.screens = screens;
	}

	
	
	
	
}
