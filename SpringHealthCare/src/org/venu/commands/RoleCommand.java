package org.venu.commands;

import java.util.List;

public class RoleCommand {
	
	private String roleName;
	private String roleDesc;
	private List<Integer> modules;
	
	
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getRoleDesc() {
		return roleDesc;
	}
	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}
	public List<Integer> getModules() {
		return modules;
	}
	public void setModules(List<Integer> modules) {
		this.modules = modules;
	}
	
	

}
