package org.venu.commands;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="user")
public class ValidXML {
	
	private String valid ;

	public String isValid() {
		return valid;
	}
	
	@XmlElement
	public void setValid(String valid) {
		this.valid = valid;
	}
	
	@Override
	public String toString() {
		return "ValidXML [valid=" + valid + "]";
	}


}
