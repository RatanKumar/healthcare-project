package org.venu.aspects;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/** 
 * This is example for expression Language 
 * @author venugopal
 *
 */

@Component("customerBean")
public class Customer {
	@Value("#{itemBean}")
  private Item item;
	@Value("{itemBean.name}")
  private String itemName;
public Item getItem() {
	return item;
}
public void setItem(Item item) {
	this.item = item;
}
public String getItemName() {
	return itemName;
}
public void setItemName(String itemName) {
	this.itemName = itemName;
}
@Override
public String toString() {
	return "Customer [item=" + item + ", itemName=" + itemName + "]";
}
  
	
}
