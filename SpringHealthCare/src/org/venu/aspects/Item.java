package org.venu.aspects;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
/**
 * This is an example for Expression Language
 * @author venugopal
 *
 */
@Component("itemBean")
public class Item {
     @Value("itemA")
	private String name;
	@Value("20")
     private int qty;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}
	@Override
	public String toString() {
		return "Item [name=" + name + ", qty=" + qty + "]";
	}
	
	
	
	
}
