package org.venu.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LoggingAspect {
	
	private static final Logger  LOGGER=LoggerFactory.getLogger(LoggingAspect.class); 
	
    @Before("execution(* org.venu.service.IUserService.authorizeUser(..))")
	public void logBefore(JoinPoint joinPoint){
    	LOGGER.info("Executing  {}  Method" +joinPoint.getSignature().getName());
   	}
               
 	@AfterReturning(pointcut = "execution(* org.venu.service.IUserService.authorizeUser(..))",
		            returning= "result")
	public void logafterReturning(JoinPoint joinPoint, Object result){
 	
 		  LOGGER.info("Method {}   Returning {} ",joinPoint.getSignature().getName(),result);
	    		
 		
	     }
}
